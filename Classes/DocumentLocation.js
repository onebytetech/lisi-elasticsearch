var DocumentLocation = function(from) {
	this.collection = null;
	this.id = null;
	if (from) {
		for (var key in from) {
			if (this.hasOwnProperty(key)) {
				this[key] = from[key];
			}
		}
	}
}

module.exports = DocumentLocation;