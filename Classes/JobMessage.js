var DocumentLocation = require('./DocumentLocation');
var QueryObject = require('./QueryObject');
var JobTypes = require('./JobTypes');


var JobMessage = function(from) {
	this.type = null;
	this.location = null;
	
	if (JobTypes.validateType(from['type'])) {
		this['type'] = from['type'];
	}
	var loc = null;
	if (from['location']) {
		loc = new DocumentLocation(from['location']);
	}
	if (from['query']) {
		loc = new QueryObject(from['query']);
	}
	if (loc instanceof DocumentLocation) {
		this['location'] = from['location'];
	}
	if (loc instanceof QueryObject) {
		this['query'] = from['query'];
	}
}

module.exports = JobMessage;