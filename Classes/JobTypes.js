var jobTypes = {
	index: 'INDEX',
	remove: 'REMOVE',
	search: 'SEARCH',
	searchQuery: "QRYSEARCH",
	validateType : function(type) {
		return (type === this.index || type === this.remove || type === this.search || type === this.searchQuery);
	}
}

module.exports = jobTypes;