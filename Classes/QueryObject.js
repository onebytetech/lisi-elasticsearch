var QueryObject = function(from) {
	this.queryText = null;
	this.docType = null;
	if (from) {
		for (var key in from) {
			if (this.hasOwnProperty(key)) {
				this[key] = from[key];
			}
		}
	}
}

module.exports = QueryObject;