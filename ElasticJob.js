var job = require('./job');
var handlers = require('./jobHandlers');

var jobInterval = 5 * 1000

function execJob() {
	job(handlers);
	setTimeout(execJob, jobInterval);
}
setTimeout( function() {
	console.log('Starting job.');
	execJob();
}, 0 );