var entity = require("./Entity.js");
var util = require("util");

var Asset = function (from) {
	this.location = null;
	this.description = null;
	this.tags = [];
	this.userId = null;
	this.info = {
		user: {
			username: null,
			imageUrl: null
		},
		commentStats: {
			cnt: 0,
			commenters: [] //an array of { imageUrl: null }
		},
		loveCnt: 0,
		noteCnt: 0
	};
	Asset.super_.call(this, from);
};
util.inherits(Asset, entity);

module.exports = Asset;