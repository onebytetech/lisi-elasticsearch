var util = require('util');
var entity = require('./Entity');

var Comment = function (from) {
	this.text = null;
	this.stackId = null;
	this.assetId = null;
	this.userId = null;
	this.info = {
		user: {           
			username: null,
			imageUrl: null
		},
		stats: {
		likeCount: 0,
		dislikeCount: 0
        }
	};
	Comment.super_.call(this, from);
};
util.inherits(Comment, entity);

module.exports = Comment;