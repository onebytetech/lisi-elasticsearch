var Entity = function(from) {
    this.id = null;   
    this.createdOn = null;
    this.isDeleted = false;
    if(from) {
        for(var p in from) {
            if(this.hasOwnProperty(p)){
                this[p] = from[p];
            }
        }
    }
};
module.exports = Entity;