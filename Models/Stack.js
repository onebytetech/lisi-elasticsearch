var Entity = require('./Entity');
var util = require('util');

var Stack = function (from) {
	this.title = null;
	this.description = null;
	this.tags = [];
	this.assets = [];
	this.coverImageUrl = null;
	this.userId = null;
	this.info = {
		user : {
			username: null,
			imageUrl: null
		},
		commentStats: {
			cnt: 0,
			commenters: [] //an array of { imageUrl: null }
		},
		sortStats: {
			sortedByUserIds: []
		}
	};
	Stack.super_.call(this, from);
};
util.inherits(Stack, Entity);

module.exports = Stack;