var util = require('util');
var Entity = require('./Entity');

var User = function (from) {
	this.email = null;    
	this.username = null;
	this.firstname = null;
	this.surname = null;
	this.gender = null;     //F or M
	this.password = null;
	this.dob = null;        //Date
	this.imageUrl = null;
	this.headerImageUrl = null;
	this.business = {
		businessName: null,
		description: null,
		brands: []
	};
	this.addresses = [];
	this.employerUserId = null; //A company a user works for
	User.super_.call(this, from);
};
util.inherits(User, Entity);

module.exports = User;