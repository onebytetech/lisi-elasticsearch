var fs = require('fs');
var azure = require('./lib/QueueManager');
var elkCtrl = require('./lib/ELKCtrl');
var JobTypes = require('./Classes/JobTypes');
var docGen = require('./lib/DocumentGenerator');

// The job which fetches a job type from queue and executes the relevant handler.
var job = {
	execute: function (handler) {
		azure.fetchJob(false).then(function(job) {
			handler.debug(job);
			if (job.type === JobTypes.remove) {
				elkCtrl.remove(job.location)
				.then(function(res) {
					handler.onRemoveDoc(res);
				}, function(err) {
					handler.onError(err);
				});
			} else if (job.type === JobTypes.search) {
				elkCtrl.search(job.location)
				.then(function(res) {
					handler.onSearchDoc(res);
				}, function(err) {
					handler.onError(err);
				});
			} else if (job.type === JobTypes.searchQuery) {
				elkCtrl.searchByQuery(job.query)
				.then(function(res) {
					handler.onSearchDoc(res);
				}, function(err) {
					handler.onError(err);
				});
			} else if (job.type === JobTypes.index) {
				var document = docGen.randomDoc();
				elkCtrl.index( document).then(function(res) {
					handler.onIndexDoc(res);
				}, function(err) {
					handler.onError(err);
				});
			}
		}, function(err) {
			handler.onError(err);
		});
	}
}
module.exports = job.execute;