var elasticHandler = {
	onRemoveDoc : function(res) {
		console.log('Executed job task - remove');
		console.log('Result: %j', res);
	},
	onIndexDoc : function(indxSt) {
		console.log('Executed job task - index');
		console.log('Result: %j', indxSt);
	},
	onSearchDoc : function(searchRes) {
		console.log('Executed job task - search');
		console.log('Result: %j', searchRes);
	},
	onError : function(err) {
		console.log('Error in job');
		console.log('Error: %j', err);
	},
	debug : function(info) {
		console.log('Debug Log: %j', info );
	}
}
module.exports = elasticHandler;