var User = require('../Models/User');
var Comment = require('../Models/Comment');
var Stack = require('../Models/Stack');
var Asset = require('../Models/Asset');

function getDocument(type) {
	if (type === 'user') {
		var user = new User({
			id: '1764',
			createdOn : new Date().toISOString(),
			isDeleted: false,
			email: 'test1@gmail.com',
			username: 'testuser1',
			firstname: 'john',
			surname: 'skully',
			gender: 'male',
			password: 'hashedpwd',
			dob: new Date().toISOString(),
			imageUrl: 'http://www.testimageurlcheckered.com/checkered.png',
			headerImageUrl: 'http://www.testimageurlcheckered.com/checkered@2x.png',
			business: {
				businessName: 'ubuntu',
				description: 'all your base belongs to us',
				brands: ['gucci', 'armani', 'thatsalliknow']
			},
			address: 'house 11 greenland.',
			employerUserId: '123asd1'
		})
		return user;
	}
	if (type === 'comment') {
		var comment = new Comment({
			id: '12344',
			createdOn: new Date().toISOString(),
			isDeleted: false,
			text: 'Help me this is a test comment.',
			stackId: '123451',
			assetId: '99432',
			userId: '2123user11',
			info: {
				user: {
					username: 'testuser1',
					imageUrl: 'http://www.helloworldimage.com/png'
				},
				stats: {
					likeCount: 15,
					dislikeCount: 25
				}
			}
		})
		return comment;
	}
	if (type === 'stack') {
		var stack = new Stack({
			id: '214851',
			createdOn: new Date().toISOString(),
			isDeleted: false,
			title: 'kapray',
			description: 'Clothes in urdu',
			tags: ['shalwar', 'kameez', 'topi'],
			assets: [],
			coverImageUrl: 'http://covers.images.com/image/url/for/elasic.png',
			userId: 'johndoe11',
			info : {
				user: {
					username: 'nikola',
					imageUrl: 'http://www.tesla.io/he/was/awesome.jpg',
				},
				commentStats: {
					cnt: 153,
					commenters: ['sad,','qeqwe']
				},
				sortStats: {
					sortedByUserIds: ['2134', '21345', '1948']
				}
			}
		});
		return stack;
	}
	if (type === 'asset') {
		var asset = new Asset({
			id: '213842',
			createdOn: new Date().toISOString(),
			isDeleted: false,
			location: 'idkwhere',
			description: 'very nice item with rich colors and high quality fabric',
			tags: ['clothes', 'party', 'rich', 'exotic', 'prom'],
			userId: '12394',
			info : {
				user: {
					username: 'testuser1',
					imageUrl: 'http://www.fancypath.com/graph.png' 
				},
				commentStats: {
					cnt: 21,
					commentors: ['asd21e', '21313e', 'a213easd213']
				},
				loveCnt: 23,
				noteCnt: 2149
			}
		});
		return asset;
	}
}

function getRandomDoc() {
	var rand = Math.floor((Math.random() * 4) + 1);
	switch (rand) {
		case 1:
			return getDocument('user');
		case 2:
			return getDocument('comment');
		case 3:
			return getDocument('stack');
		case 4:
			return getDocument('asset');
	}
}

var DocumentGenerator = {
	randomDoc : getRandomDoc,
	getDoc : getDocument
}

module.exports = DocumentGenerator;