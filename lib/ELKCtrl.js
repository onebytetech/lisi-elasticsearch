var Q = require('q');
var elasticsearch = require('elasticsearch');
var QueryObject = require('../Classes/QueryObject');
var DocumentLocation = require('../Classes/DocumentLocation');
var elasticConfig = require('../config').elasticConn;

// Data models
var User = require('../Models/User');
var Stack = require('../Models/Stack');
var Comment = require('../Models/Comment');
var Asset = require('../Models/Asset');

// Convert an elasticsearch returned object to Model object
function ELKToObject(ELKDoc) {
	
}
// Get Index reference for a specified Model object
function getIndexRef(dataModel) {
	var refObj = {
		index: elasticConfig.index,
		id: dataModel.id,
		body: dataModel
	}
	if (dataModel instanceof User) {
		refObj.type = 'user'
	}
	if (dataModel instanceof Stack) {
		refObj.type = 'stack'
	}
	if (dataModel instanceof Comment) {
		refObj.type = 'comment'
	}
	if (dataModel instanceof Asset) {
		refObj.type = 'asset';
	}
	if (refObj.type) {
		return refObj;
	}
	return null;
}

// Controller to handle all elasticsearch related operations.
var ELKCtrl = {
	// Search(fetch) a document specified by @param documentLocation from elasticsearch index.
	search : function(documentLocation) {
		var self = this;
		documentLocation = new DocumentLocation(documentLocation);
		var promise = new Q.Promise(function(resolve, reject) {
			if (!(documentLocation instanceof DocumentLocation)) {
				reject('Invalid Document Location.');
			}
			var client = self._client;
			var searchObj = {
				index: elasticConfig.index,
				type: documentLocation.collection,
				id: documentLocation.id,
			};
			client.search(searchObj)
			.then(function(document) {
				resolve(document);
			}, function(err) {
				reject(err);
			});
		})
		return promise;
	},
	searchByQuery: function(queryObj) {
		var self = this;
		queryObj = new QueryObject(queryObj);
		var promise = new Q.Promise(function(resolve, reject) {
			if (! (queryObj instanceof QueryObject)) {
				reject('Invalid Query Object');
			}
			var client = self._client;
			var searchObj = {
				index: elasticConfig.index,	
			};
			if (queryObj.queryText instanceof Object ) {
				searchObj.body = {};
				searchObj.body.query = queryObj.queryText;
			} else {
				searchObj.q = queryObj.queryText;
				searchObj.type = queryObj.docType;
			}
			client.search(searchObj)
			.then(function(document) {
				resolve(document);
			}, function(err) {
				reject(err);
			});
		});
		return promise;
	},
	// Index an instance of object @param dataObj in Controllers to elasticsearch index.
	index: function(dataObj) {
		var self = this;
		var reference = getIndexRef(dataObj);
		var promise = new Q.Promise(function(resolve, reject) {
			if (reference === null) {
				reject('dataObj does not belong to defined Data Models.');
			}
			var client = self._client;
			client.index(reference)
			.then(function(indRes) {
				resolve(indRes);
			}, function(err) {
				reject(err);
			})
		})
		return promise;
	},
	// Removed a document specified by @param documentLocation from elasticsearch index.
	remove: function(documentLocation) {
		documentLocation = new DocumentLocation(documentLocation);
		var self = this;
		var promise = new Q.Promice(function(resolve, reject) {
			if (!(documentLocation instanceof DocumentLocation)) {
				reject('Invalid Document Location.');
			}
			var client = self._client;
			var deleteObj = {
				index: elasticConfig.index,
				type: documentLocation.collection,
				id: documentLocation.id
			};
			client.delete(deleteObj)
			.then(function(delRes) {
				resolve(delRes);
			}, function(err) {
				reject(err);
			});
		})
		return promise;
	}
};

ELKCtrl._client = new elasticsearch.Client(elasticConfig);

module.exports = ELKCtrl;