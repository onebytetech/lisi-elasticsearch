/*
 * QueueManager.js
 * Handles Queue Messaging (Push and Pulls) for Image Resizing job.
 */
var Q = require('q');
var util = require('util');
var azure = require('azure-storage');

var DocumentLocation = require('../Classes/DocumentLocation');
var JobMessage = require('../Classes/JobMessage');


var ELASTIC_JOB_QUEUE = 'elasticjobs'; //Queue name where elastic search jobs would reside.

// var emuString = require('./AzureEmulator').getEmulatorConnString();
var queueService = azure.createQueueService();
var QueueManager = {};

function verifyMsgObj(obj) {
	return (obj instanceof JobMessage);
}

function createQueue(queueName) {
	var promise = new Q.Promise(function(resolve, reject) {
		queueService.createQueueIfNotExists(queueName, function(error, createdObj) {
			if (error) {
				reject(error);
			} else {
				resolve(createdObj);
			}
		});	
	})
	return promise;
}

/*
 * addJob - Adds a job in the Azure jobs queue 
 * @param documentLoc - [DocumentLocation] - Location object identifying collection name and id of the document to be searched.
 * Returns Promise
 */
QueueManager.addJob = function addJob(jobMsg) {
	// Verify Valid params
	var promise = new Q.Promise(function(resolve, reject) {
		if ( !(verifyMsgObj(jobMsg))) {
			reject('QueueManager.addJob - Invalid JOB_MESSAGE object.');
		}
		createQueue(ELASTIC_JOB_QUEUE)
		.then(function(createdObj) {
			var jobObj = JSON.stringify(jobMsg);	
			queueService.createMessage(ELASTIC_JOB_QUEUE, jobObj, function(error) {
				if (error) {
					return reject(error);
				}
				resolve({msg: 'Added a new job entry'});
			})
		}, function(err) {
			reject(err);
		})
	});
	return promise;
}

/*
 * fetchJob - Fetches an elastic job from the job queue.
 * @param keep - String - Determines if the job should be kept in the queue after fetching.
 */
QueueManager.fetchJob = function fetchJob(keep) {
	console.log('Received Fetch job request.');
	var promise = new Q.Promise(function (resolve, reject) {
		createQueue(ELASTIC_JOB_QUEUE)
		.then(function(createdObj) {
			queueService.getMessages(ELASTIC_JOB_QUEUE, {numOfMessages: 1}, function(error, queueMsgs) {
				if (error) {
					return reject(error);
				}
				var latestMsg = queueMsgs[0];
				var totalMsgs = queueMsgs.length;
				// console.log(util.inspect(queueMsgs));
				if (!latestMsg) {
					return resolve('Queue is Empty.');
				}
				var resizeJob = new JobMessage(JSON.parse(latestMsg.messagetext));
				if (keep) {
					return resolve(resizeJob, totalMsgs);
				}
				queueService.deleteMessage(ELASTIC_JOB_QUEUE, latestMsg.messageid, latestMsg.popreceipt, function(err) {
					if (err) {
						return reject(err);
					}
					resolve(resizeJob, totalMsgs);
				});
			});		
		}, function(err) {
			reject(err);
		});
	});
	return promise;
}

module.exports = QueueManager;