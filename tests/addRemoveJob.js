var queueManager = require('../lib/QueueManager');
var DocumentLocation = require('../Classes/DocumentLocation');
var JobMessage = require('../Classes/JobMessage');
var JobTypes = require('../Classes/JobTypes');

var docLoc = new DocumentLocation({collection: 'Stack', id: '213842'});
var msg = new JobMessage({type: 'REMOVE', location: docLoc});
console.log('Adding job...');
console.log(msg);

queueManager.addJob(msg)
.then(function(res) {
	console.log(res);
}, function(err) {
	console.log(err);
})