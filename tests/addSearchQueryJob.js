var queueManager = require('../lib/QueueManager');
var QueryObject = require('../Classes/QueryObject');
var JobMessage = require('../Classes/JobMessage');
var JobTypes = require('../Classes/JobTypes');

var queryObj = new QueryObject({queryText: {match: {title: 'clothes'}}, docType: 'Stack'});
var msg = new JobMessage({type: 'QRYSEARCH', query: queryObj});
console.log('Adding job...');
console.log(msg);

queueManager.addJob(msg)
.then(function(res) {
	console.log(res);
}, function(err) {
	console.log(err);
})