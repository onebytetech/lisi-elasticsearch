var queueManager = require('../lib/QueueManager');
var DocumentLocation = require('../Classes/DocumentLocation');
var JobMessage = require('../Classes/JobMessage');
var JobTypes = require('../Classes/JobTypes');

var docLoc = new DocumentLocation({collection: 'Stack', id: '123asd122'});
var msg = new JobMessage({type: 'INDEX', location: docLoc});

var KeepJob = true;

queueManager.fetchJob(KeepJob).then(function(job) {
	console.log(job);
}, function(err) {
	console.error(err);
})