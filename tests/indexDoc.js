var elkMan = require('../lib/ELKCtrl');
var docGen = require('../lib/DocumentGenerator');

// Gets a document object based on passed string value name of object class.

var asset1 = docGen.randomDoc();

console.log(asset1);

elkMan.index(asset1)
.then(function(result) {
	console.log(result);
}, function(err) {
	console.error(err);
});