// searchDoc.js - Searched a hardcoded / passed in command line document in Elastic Cluster defined in config file.
var QueryObject = require('../Classes/QueryObject');
var elkMan = require('../lib/ELKCtrl');

var queryText = process.argv.slice(2)[0] || 'shoe';
var docType = process.argv.slice(3)[0] || 'asset';

var queryObject = new QueryObject({queryText: queryText, docType: docType});

elkMan.searchByQuery(queryObject).then(function(result) {
	console.log(result);
	console.log("Query Result: %j", result);
}, function(err) {
	console.error(err);
});