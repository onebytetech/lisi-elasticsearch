// searchDoc.js - Searched a hardcoded / passed in command line document in Elastic Cluster defined in config file.
var DocumentLocation = require('../Classes/DocumentLocation');
var elkMan = require('../lib/ELKCtrl');

var id = process.argv.slice(2)[0] || '213842';
var collection = process.argv.slice(3)[0] || 'asset';

var loc = new DocumentLocation({id: id, collection : collection});
elkMan.search(loc).then(function(result) {
	console.log(result);
	console.log('Object: %j', result.hits);
}, function(err) {
	console.error(err);
});