// testElastic.js - Tests Connection to Elastic Search Cluster. Link defined in config file.
var elkMan = require('../lib/ELKCtrl');

elkMan._client.ping({requestTimeout: Infinity, hello: 'elasticSearch!'})
.then(function(res) {
	console.log(res);
}, function(err) {
	console.log(err);
})